# Test app

A JSON API service developed in Sinatra, Ruby.

### 1. Start projects
--------------------

Clone the project repo

    $ git clone https://gitlab.com/Bodyk1Love/bepaid_testapp.git

Run

    $ docker-compose up --build


### 2. Database Setup
--------------------
With running docker container run

    $ docker-compose exec web bundle exec rake db:create 
    $ docker-compose exec web bundle exec rake db:migrate
    $ docker-compose exec web bundle exec rake db:seed 

### 3. Dev link
--------------------
Default dev link is

    http://localhost:4567/


### 4. Available routes
--------------------
Routing

    post    /posts         - create post
    get     /posts/top/3   - get top posts by average rating
    post    /ratings       - create rating
    get     /ips/by_user   - get groupped data with ip and users posting from it
