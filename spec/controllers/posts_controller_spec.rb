require 'spec_helper'

describe PostsController, type: :controller do
  let(:app) {PostsController.new}

  describe "create posts" do
    let(:params) { attributes_for(:post) }

    context "with valid params" do
      it "return status 200 and post attributes" do
        post "/posts", params.merge(author: "hehe")
        expect(last_response.status).to eq 200
        response = JSON.parse(last_response.body)["data"]
        expect(response).to include params.stringify_keys
      end
      
    end

    context "with unvalid params" do
      it "return status 422 and validation error" do
        post "/posts", params
        expect(last_response.status).to eq 422
        response = JSON.parse(last_response.body)["errors"]
        expect(response).to include({"author": "not provided"}.stringify_keys)
      end
    end
  end

  describe "side actions" do
    let(:params) { attributes_for(:post) }

    context "with user that exist" do
      let(:user) { create(:user, login: "hehe") }
      it "should not create a new one" do
        post "/posts", params.merge(author: user.login)
        response = JSON.parse(last_response.body)["data"]
        expect(response["user_id"]).to eq(user.id)
      end
      
    end

    context "with user that does not exist" do
      it "should create a new one" do
        expect{ post "/posts", params.merge(author: "brand new") }.to change{ User.count }.by(1)
      end
    end

    context "new ip log created" do
      it "should create a new one" do
        expect{ post "/posts", params.merge(author: "brand new") }.to change{ IpLog.count }.by(1)
      end
    end
  end
end

