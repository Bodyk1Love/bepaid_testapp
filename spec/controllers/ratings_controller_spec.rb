require 'spec_helper'

describe RatingsController, type: :controller do
  let(:app) {RatingsController.new}

  describe "create rating" do
    let(:my_post) { create(:post) }

    context "with valid params" do
      it "return status 200 and post average rating" do
        post "/ratings", {'post_id': my_post.id, value: '5'}
        expect(last_response.status).to eq 200
        response = JSON.parse(last_response.body)["data"]
        my_post.reload
        expect(response).to eq(my_post.average_rating)
      end
    end

    context "with unvalid params" do
      it "return status 422 and validation error" do
        post "/ratings", {'post_id': my_post.id, value: 100}
        expect(last_response.status).to eq 422
        response = JSON.parse(last_response.body)["errors"]
        expect(response).to include({"value": "not in 1..5"}.stringify_keys)
      end
    end
  end

end

