require 'spec_helper'

describe IpLogsController, type: :controller do
  let(:app) {IpLogsController.new}

  describe "get logs" do
    context 'with logs but only one' do
      it "return status 200 and empty list" do
        create(:ip_log)
        get "/ips/by_user"
        expect(last_response.status).to eq 200
        response = JSON.parse(last_response.body)["data"]
        expect(response).to eq([])
      end
    end

    context 'with multiple user from one ip' do
      it "return status 200 and empty list" do
        create(:ip_log)
        create(:ip_log, user_login: 'hehe1')
        get "/ips/by_user"
        expect(last_response.status).to eq 200
        response = JSON.parse(last_response.body)["data"]
        expect(response.length).to eq(1)
        expect(response[0]['authors']).to include("hehe1", "hehe")
      end
    end
  end

end

