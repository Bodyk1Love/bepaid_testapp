require 'pg'
require 'pry'
require 'rspec'
require 'sinatra'
require 'rack/test'
require 'shoulda/matchers'
require './spec/support/factory_bot'
require './spec/support/database_cleaner'
require './config/environment'


RSpec.configure do |config|
  config.include(Shoulda::Matchers::ActiveModel, type: :model)
  config.include(Shoulda::Matchers::ActiveRecord, type: :model)
  config.include Rack::Test::Methods
end
