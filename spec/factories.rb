FactoryBot.define do
  factory :ip_address do
    value { "172.16.0.0" }
  end

  factory :user do
    login { "hehe" }
  end

  factory :ip_log do
    user_id { 1 }
    ip_address_id { 1 }
    user_login { 'hehe'}
    ip  { '123' }
  end

  factory :post do
    title { "Test" }
    content  { "This is content" }
    user
    ip_address
  end

  factory :rating do
    post
    value { 5 }
  end
end