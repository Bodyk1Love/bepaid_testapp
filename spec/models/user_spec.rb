require 'spec_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:login) }
  end
end
