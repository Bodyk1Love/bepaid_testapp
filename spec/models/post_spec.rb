require 'spec_helper'

RSpec.describe Post, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:content) }
  end

  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:ip_address) }
    it { should respond_to(:ratings) }
  end

  describe 'after create' do
    it 'should create ip_log' do
      expect {create(:post)}.to change{ IpLog.count }.by(1)
    end
  end
end
