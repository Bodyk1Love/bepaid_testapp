require 'spec_helper'

RSpec.describe IpAddress, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:value) }
  end
end
