require 'spec_helper'

RSpec.describe IpLog, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:ip) }
    it { should validate_presence_of(:user_login) }
  end

  describe 'associations' do
    it { should belong_to(:user) }
    it { should belong_to(:ip_address) }
  end
end
