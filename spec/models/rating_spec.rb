require 'spec_helper'

RSpec.describe Rating, type: :model do
  describe 'validations' do
    it { should validate_presence_of(:value) }
  end

  describe 'associations' do
    it { should belong_to(:post) }
  end

  describe 'after create' do
    let(:post) { create(:post) }
    it 'should update posts average rate' do
      create(:rating, post: post, value: 5)
      create(:rating, post: post, value: 1)
      expect(post.average_rating).to eq(3)
    end
  end
end
