require 'net/http'
require 'sinatra'

class Requester
  API_URl = Sinatra::Base.production? ? 'http://bepaid.herokuapp.com' : 'http://localhost:4567'

  def call(type:, endpoint:, body: {})
    
    case type
    when :post
      res = Net::HTTP.post_form(URI.parse(API_URl + endpoint), body)
    when :get
      Net::HTTP.get(URI.parse(API_URl + endpoint))
    end
  end
end