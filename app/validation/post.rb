module PostValidation
  class Create
    include BaseValidation

    def perform
      validate do
        attribute :title,   String, required: true
        attribute :content, String, required: true
        attribute :author,  String, required: true
        attribute :ip,      String, required: true
      end
    end
  end

  class Top
    include BaseValidation

    def perform
      validate do
        attribute :n, Integer, required: true
      end
    end
  end
end
