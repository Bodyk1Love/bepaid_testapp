module RatingValidation
  class Create
    include BaseValidation

    def perform
      validate do
        attribute :post_id, Integer, required: true
        attribute :value,   Integer, required: true, in: 1..5
      end
    end
  end
end
