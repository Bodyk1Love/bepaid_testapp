class IpLogsController < BaseController
  get '/ips/by_user' do
    perform(operation: operation(:by_user).new(params), validate: false) { 200 }
  end
end
