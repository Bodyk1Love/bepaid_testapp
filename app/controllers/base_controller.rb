require 'sinatra/base'

class BaseController < Sinatra::Base
  def operation(type)
    "#{resource_name}Operation::#{type.to_s.classify}".constantize
  end

  def validation(type)
    "#{resource_name}Validation::#{type.to_s.classify}".constantize
  end

  def resource_name
    self.class.name.split('::').last.sub('Controller', '').singularize
  end

  def perform(operation:, validate: true, &block)
    result = operation.process(validation: validate && validation(operation.class.name.demodulize))
    status = block.call
    if operation.errors.empty?
      { status: status, data: result }.to_json
    else
      halt 422, { status: 422, errors: operation.errors }.to_json
    end
  end
end
