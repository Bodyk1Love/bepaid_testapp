class PostsController < BaseController
  post '/posts' do
    perform(operation: operation(:create).new(params.merge(ip: request.ip))) { 200 }
  end

  get '/posts/top/:n' do
    perform(operation: operation(:top).new(params)) { 200 }
  end
end
