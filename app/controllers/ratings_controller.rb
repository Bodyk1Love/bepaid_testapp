class RatingsController < BaseController
  post '/ratings' do
    perform(operation: operation(:create).new(params)) { 200 }
  end
end
