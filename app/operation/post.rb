module PostOperation
  class Create
    include BaseOperation

    def perform
      user = User.find_or_create_by(login: @form[:author])
      ip = IpAddress.find_or_create_by(value: @form[:ip])
      @model.create(
        title: @form[:title],
        content: @form[:content],
        user: user,
        ip_address: ip
      )
    end
  end

  class Top
    include BaseOperation

    def perform
      Post.order(average_rating: :desc).select(:title, :content).limit(@form[:n])
    end
  end
end
