module RatingOperation
  class Create
    include BaseOperation

    def perform
      unless post = Post.find_by(id: @form[:post_id])
        add_error!('post', 'not exist')
        return
      end
      @model.create(
        value: @form[:value],
        post: post
      )
      post.reload
      post.average_rating
    end
  end
end
