module IpLogOperation
  class ByUser
    include BaseOperation

    def perform
      data = IpLog.distinct.pluck(:user_login, :ip)
      groupped = data.inject({}) do |result, (login, ip)|
        result[ip] ||= []
        result[ip] << login
        result
      end
      groupped.map { |k, v| { ip: k, authors: v } if v.length > 1 }.compact
    end
  end
end
