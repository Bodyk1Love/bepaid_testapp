class Post < ActiveRecord::Base
  validates_presence_of :title, :content, :user_id

  belongs_to :user
  belongs_to :ip_address

  has_many :ratings

  after_create :create_ip_log

  def create_ip_log
    IpLog.create(
      user: user,
      ip_address: ip_address,
      ip: ip_address.value,
      user_login: user.login
    )
  end
end
