class IpAddress < ActiveRecord::Base
  validates_presence_of :value
end
