class IpLog < ActiveRecord::Base
  validates_presence_of :ip, :user_login

  belongs_to :user
  belongs_to :ip_address
end
