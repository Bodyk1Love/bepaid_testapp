class Rating < ActiveRecord::Base
  validates_presence_of :value

  belongs_to :post

  after_save :calculate_rating

  def calculate_rating
    Post.with_advisory_lock('average_rating_update') do
      post.update(average_rating: post.ratings.average(:value).round(1))
    end
  end
end
