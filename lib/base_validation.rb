module BaseValidation
  attr_reader :form, :params

  def initialize(params = {})
    @params = params
    @form = {}
    @errors = {}
  end

  def perform; end

  def attribute(name, type, options = {})
    raise TypeError unless [Integer, Float, String].include?(type)

    value = coerce(params[name], type, name)
    check_presence(name, value) if options[:required]
    check_inclusion(name, value, options[:in]) if options[:in]
    form[name] = value
  end

  def add_error!(code, message)
    @errors[code] = message
  end

  def validate
    yield if block_given?
    [form.symbolize_keys, @errors]
  end

  private

  def check_presence(name, value)
    add_error!(name, 'not provided') unless value
  end

  def check_inclusion(name, value, range)
    raise TypeError unless range.is_a?(Enumerable)

    add_error!(name, "not in #{range}") unless range.include?(value)
  end

  def coerce(param, type, name)
    return param if param.nil?
    return param if param.is_a?(type)

    begin
      return Integer(param, 10) if type == Integer
      return Float(param) if type == Float
      return String(param) if type == String
    rescue ArgumentError
      add_error!(name, 'wrong type')
    end
  end
end
