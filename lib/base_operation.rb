module BaseOperation
  attr_reader :form, :model, :result, :params, :errors

  def initialize(original_params = {})
    original_params = original_params.to_h if original_params.is_a?(Sinatra::IndifferentHash)
    @params = original_params.dup.with_indifferent_access
    @model = self.class.parent.to_s.delete_suffix('Operation').constantize
    @errors = {}
  end

  def process(**args)
    setup_validation!(args[:validation]) if args[:validation]
    return unless errors.empty?

    @result = perform
  end

  def perform; end

  def add_error!(code, message)
    @errors[code] = message
  end

  protected

  def setup_validation!(validator = nil)
    @form, err = validator.new(params).perform
    errors.merge! err
  end
end
