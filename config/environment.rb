ENV['RACK_ENV'] ||= 'development'

require 'bundler/setup'
Bundler.require(:default, ENV['RACK_ENV'])

require 'uri'
require 'pathname'
require 'active_record'
require 'logger'
require 'faker'
require 'sinatra'
require 'sinatra/activerecord'
require 'sinatra/reloader' if development?
require 'erb'
require 'require_all'

require_all 'lib'
require_all 'app'
require_all 'services'
