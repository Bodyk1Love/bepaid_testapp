require './config/environment'

set :app_file, __FILE__

run Sinatra::Application
use Rack::MethodOverride
run BaseController
use PostsController
use RatingsController
use IpLogsController
