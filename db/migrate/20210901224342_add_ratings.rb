class AddRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.integer :value, null: false, default: 1
      t.references :post, null: false
    end
  end
end
