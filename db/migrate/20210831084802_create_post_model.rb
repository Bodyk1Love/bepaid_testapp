class CreatePostModel < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title, null: false
      t.string :content, null: false
      t.float :average_rating, default: 0.0, null: false
    end
  end
end
