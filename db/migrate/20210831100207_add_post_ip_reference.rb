class AddPostIpReference < ActiveRecord::Migration
  def change
    add_reference :posts, :user, index: true, null: false
    add_reference :posts, :ip_address, index: true, null: false
  end
end
