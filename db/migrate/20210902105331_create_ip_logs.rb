class CreateIpLogs < ActiveRecord::Migration
  def change
    create_table :ip_logs do |t|
      t.references :ip_address, null: false
      t.references :user, null: false
      t.string :ip, null: false
      t.string :user_login, null: false
      t.datetime :created_at, null: false, default: DateTime.now
    end
  end
end
