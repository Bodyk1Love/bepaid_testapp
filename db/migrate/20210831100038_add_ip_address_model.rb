class AddIpAddressModel < ActiveRecord::Migration
  def change
    create_table :ip_addresses do |t|
      t.string :value, null: false
    end
  end
end
