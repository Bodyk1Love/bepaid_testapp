require 'faker'

# Create posts
100.times.each do
  body = {
    title: Faker::Hipster.sentences.sample,
    content: Faker::Quote.matz,
    author: Faker::Internet.username,
    ip: Faker::Internet.ip_v4_address
  }
  Requester.new.call(type: :post, endpoint: '/posts', body: body)
end


# Create ratings
ids = Post.pluck(:id)
100.times.each do
  body = {post_id: ids.sample, value: rand(1..5)}
  Requester.new.call(type: :post, endpoint: '/ratings', body: body)
end