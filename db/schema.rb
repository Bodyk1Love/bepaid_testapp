# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_210_902_105_331) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'ip_addresses', force: :cascade do |t|
    t.string 'value', null: false
  end

  create_table 'ip_logs', force: :cascade do |t|
    t.integer  'ip_address_id',                                 null: false
    t.integer  'user_id',                                       null: false
    t.string   'ip',                                            null: false
    t.string   'user_login',                                    null: false
    t.datetime 'created_at', default: '2021-09-02 11:06:53', null: false
  end

  create_table 'posts', force: :cascade do |t|
    t.string  'title',                        null: false
    t.string  'content',                      null: false
    t.float   'average_rating', default: 0.0, null: false
    t.integer 'user_id',                      null: false
    t.integer 'ip_address_id',                null: false
  end

  add_index 'posts', ['ip_address_id'], name: 'index_posts_on_ip_address_id', using: :btree
  add_index 'posts', ['user_id'], name: 'index_posts_on_user_id', using: :btree

  create_table 'ratings', force: :cascade do |t|
    t.integer 'value', default: 1, null: false
    t.integer 'post_id', null: false
  end

  create_table 'users', force: :cascade do |t|
    t.string 'login'
  end
end
